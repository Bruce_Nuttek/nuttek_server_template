package com.nuttek.hy.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Salesman {
    private int id;
    private String name;
}
