package com.nuttek.hy.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class JwtAuthenticationToken extends AbstractAuthenticationToken {

    private String jwt;
    private JwtPrincipal principal;

    public JwtAuthenticationToken(String jwt) {
        super(null);
        this.jwt = jwt;
        setAuthenticated(false);
    }

    public JwtAuthenticationToken(String jwt, Collection<? extends GrantedAuthority> authorities,
                                  JwtPrincipal principal) {
        super(authorities);
        this.jwt = jwt;
        this.principal = principal;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return jwt;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
        jwt = null;
    }

}
