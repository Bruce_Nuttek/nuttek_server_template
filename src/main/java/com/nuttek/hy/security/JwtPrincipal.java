package com.nuttek.hy.security;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class JwtPrincipal {

    @Getter
    private String jwt;

    @Getter
    private int userId;

}
