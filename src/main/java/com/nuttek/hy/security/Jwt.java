package com.nuttek.hy.security;

import org.springframework.security.core.authority.AuthorityUtils;

import java.security.Key;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class Jwt {

    private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;

    private static final String SIGN_KEY = "TODO";

    public static String createJwt(String jwtId, String issuer, String subject, long ttlMillis) {

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        byte[] apiKeySecretBytes = SIGN_KEY.getBytes();
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, SIGNATURE_ALGORITHM.getJcaName());

        JwtBuilder builder = Jwts.builder().setId(jwtId)
                .setIssuedAt(now)
                .setSubject(subject)
                .setIssuer(issuer)
                .signWith(SIGNATURE_ALGORITHM, signingKey);

        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public static JwtAuthenticationToken parseJwt(String jwt) {
        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(SIGN_KEY.getBytes())
                .parseClaimsJws(jwt).getBody();
        int userId = Integer.valueOf(claims.getSubject());
        String role = claims.get("role", String.class);
        JwtPrincipal principal = new JwtPrincipal(jwt, userId);
        return new JwtAuthenticationToken(jwt, AuthorityUtils.createAuthorityList(role), principal);
    }

}
