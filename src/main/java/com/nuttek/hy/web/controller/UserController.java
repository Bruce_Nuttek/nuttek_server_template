package com.nuttek.hy.web.controller;

import com.nuttek.hy.domain.Salesman;
import com.nuttek.hy.service.UserService;
import com.nuttek.hy.web.dto.LoginRequest;
import com.nuttek.hy.web.dto.LoginResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public LoginResponse login(@RequestBody @Valid LoginRequest request) {
        return userService.login(request);
    }

    @GetMapping(value = "/users")
    @Cacheable("users")
    public ResponseEntity<List<Salesman>> users(WebRequest request) {
        long lastModified = 1;

        if (request.checkNotModified(lastModified)) {
            return null;
        }

        List<Salesman> salesmen = new ArrayList<>();
        return ResponseEntity.ok().cacheControl(CacheControl.maxAge(1, TimeUnit.DAYS)).lastModified(1).body(salesmen);
    }

}
