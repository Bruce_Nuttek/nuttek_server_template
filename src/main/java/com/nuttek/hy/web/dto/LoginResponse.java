package com.nuttek.hy.web.dto;

import com.nuttek.hy.domain.Salesman;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LoginResponse {

    private Salesman salesman;
    private String accessToken;

}
