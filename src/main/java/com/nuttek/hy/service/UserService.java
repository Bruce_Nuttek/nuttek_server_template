package com.nuttek.hy.service;

import com.nuttek.hy.domain.Salesman;
import com.nuttek.hy.web.dto.LoginRequest;
import com.nuttek.hy.web.dto.LoginResponse;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserService {

    public LoginResponse login(LoginRequest request) {
        System.out.println("User Login:" + request.getSalesmanJobNumber());
        LoginResponse response = new LoginResponse();
        response.setAccessToken(UUID.randomUUID().toString());

        Salesman salesman = new Salesman();
        salesman.setId(1);
        salesman.setName("Jack");

        response.setSalesman(salesman);

        return response;
    }

}
