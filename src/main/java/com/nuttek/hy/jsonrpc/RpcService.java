package com.nuttek.hy.jsonrpc;

import com.googlecode.jsonrpc4j.JsonRpcError;
import com.googlecode.jsonrpc4j.JsonRpcErrors;
import com.googlecode.jsonrpc4j.JsonRpcService;
import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import com.nuttek.hy.exception.ObjectNotFoundException;
import com.nuttek.hy.service.UserService;
import com.nuttek.hy.web.dto.LoginRequest;
import com.nuttek.hy.web.dto.LoginResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@JsonRpcService("/rpc/rpcService")
@AutoJsonRpcServiceImpl
public class RpcService {

    @Autowired
    private UserService userService;

    @JsonRpcErrors({
            @JsonRpcError(exception = ObjectNotFoundException.class,
                    code = -5678, message = "User not found", data = "The Data")
    })
    public LoginResponse login(String salesmanJobNumber, String password) {
        LoginRequest request = new LoginRequest(salesmanJobNumber, password);
        return userService.login(request);
    }

}
