package com.nuttek.config;

import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.eviction.lru.LruEvictionPolicy;
import org.apache.ignite.cache.spring.SpringCacheManager;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.events.EventType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CachingConfig {

    @Bean
    public SpringCacheManager cacheManager() {
        SpringCacheManager cacheManager = new SpringCacheManager();
        cacheManager.setGridName("hy");

        IgniteConfiguration ignite = new IgniteConfiguration();
        // ignite performance tips
        ignite.setIncludeEventTypes(EventType.EVTS_CACHE);

        CacheConfiguration cacheConfiguration = new CacheConfiguration();
        cacheConfiguration.setStartSize(100 * 1024 * 1024); // 100M
        cacheConfiguration.setCacheMode(CacheMode.PARTITIONED);
        cacheConfiguration.setBackups(0); // TODO
        cacheConfiguration.setOffHeapMaxMemory(0); // Enable off-heap storage with unlimited size
        cacheConfiguration.setSwapEnabled(false);
        cacheConfiguration.setEvictionPolicy(new LruEvictionPolicy(1000000)); // max cache size

        ignite.setCacheConfiguration(cacheConfiguration);

        cacheManager.setConfiguration(ignite);

        return cacheManager;
    }

}
