package com.nuttek.config;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImplExporter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RpcConfig {

    @Bean
    public AutoJsonRpcServiceImplExporter jsonRpcExporter() {
        AutoJsonRpcServiceImplExporter exporter = new AutoJsonRpcServiceImplExporter();
        // TODO
        // exporter.setErrorResolver(...);
        return exporter;
    }

}
