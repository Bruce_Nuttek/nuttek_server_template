template:

-url routing
-json rpc
micro services(implement, deploy)
caching(last-modified, etag)
authentication(access token, secure cookie)
-customized json(snake case)
DAO read-write separation, multiple database/table
logging(error log, access log)
pagination
http status code exception mapping(with error code, error message)